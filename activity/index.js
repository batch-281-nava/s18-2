
let hello
console.log("Hello,World");

// 1
// addition

function addition(num1 , num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum)
}
addition(5,10)

function subtraction(num1 , num2){
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference)
}
subtraction(20,5)


// 2
// Multiplication

function multiplication(num1, num2){
    let product = num1 * num2;
    console.log("The product of " + num1 + " and " + num2);
    return product;
}
const product = multiplication(50,10);
console.log(product)

function division(num1, num2){
    let quotient = num1 / num2;
    console.log("The quotient of " + num1 + " and " + num2);
    return quotient ;
}
const quotient = division(50,10);
console.log(quotient)


// 3
// circle
let radius = 15;
let pi = Math.PI;
let areaOfCircle = (radius, pi) => {
   return pi * radius * radius;
};
console.log("The result of getting the area of a circle with: " + radius + " radius" );
console.log (areaOfCircle(radius, pi));



// 4
// total average
function averageVar(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	console.log("The average of " + num1 + ","  + num2 + ","  + num3 + ","  + "and " + num4);
	return average;
}
const average = averageVar(20,40,60,80);
console.log(average);

// 5
// passing score
function isPassingScore(score, total){
    let percentage = score / total;
    console.log("Is " + score + "/" + total + " a passing score?")
    let isPassed = percentage >= .75;
    console.log(isPassed);
    return percentage;
}

let myScore = isPassingScore(38, 50);
console.log(myScore)